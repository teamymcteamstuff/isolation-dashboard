#!/usr/bin/env python
# coding: utf-8

# ###To do
# 
# 0. Make class for dashboard
# 
# 1. Add in event for drop down change
# 
# 2. Change the newsapi api_module to include the option to change chosen news sites
# 
# 3. After the above, add in checkboxes per news site to add in a headline per site when each check box is ticked
# 
# 4. Handler for no results from headlines api query
# 
# 5. Put in url links into the output
# 
# 6. Add in disabled attribute on widgets for when query is running so that widgets can't be interacted with during a query

# In[2]:


# Initialisations

import os, sys

# Only need below if running this module by itself
#repo_folder=os.path.dirname(os.path.dirname(os.path.dirname(os.getcwd())))
#sys.path.append(repo_folder)
#print('appending:', repo_folder, '- to system path')

from api_modules.newsapi import newsapi
from ipywidgets.widgets import *
from IPython.display import clear_output


# In[4]:


#### debug
borders = 'none'


# In[3]:


#### Generating values from function to put into the dashboard (e.g. for main vars)

#sites = newsapi.headlines_summary(country='gb', query='corona',view = 'sites')

#snapshot = newsapi.headlines_summary(country='gb', query='corona',view = 'snapshot')


# In[5]:


#Dashboard

class Dashboard:
    
    def __init__(self):
    
        ###### Layout helper properties

        widg_layout_genericitem = Layout(
            width='auto'
        )

        widg_style_desc = {'description_width':'initial'}

        ##### Individual Widgets

        #Title

        newsapi_title = HTML("<h1>News Headlines</h1>", layout=Layout(border=borders))

        # Perd image
        file_perd = open(os.path.dirname(__file__) + "/perd-hapley.jpg", "rb")
        image_perd = file_perd.read()
        perd = Image(
            value=image_perd,
            format='jpg',
            width=300,
            height=400,
            layout=Layout(border='')
        )

        # Enter query string widget
        widg_txtbx_querystring = Text(
            value="corona",
            description="Query string:",
            placeholder="e.g. corona",
            style = widg_style_desc,
            layout = Layout(width='200px')
        )

        # Query string run button
        widg_but_querystring = Button(
            description="Search",
            button_style="success"
        )

        # Number of sites drop down

        sites = newsapi.headlines_summary(country='gb', query='corona',view = 'sites')

        widg_dropdown_sitesnum = Dropdown(
            options=[str(i) for i in range(1,len(sites)+1)],
            value='5',
            description='Number of headlines:',
            layout = widg_layout_genericitem,
            style = widg_style_desc
        )

        # Headlines summary Snapshot view widget

        snapshot = newsapi.headlines_summary(view = 'snapshot', country='gb', query=widg_txtbx_querystring.value, view_snapshot_sitelimit=int(widg_dropdown_sitesnum.value)).drop(['url'], axis=1)

        def snapshot_HTML_val(query, view_snapshot_sitelimit):
            df = newsapi.headlines_summary(view = 'snapshot', country='gb', query=query, view_snapshot_sitelimit=view_snapshot_sitelimit).drop(['url'], axis=1)
            html_ver = df.to_html(
                index = False,
                escape = False
            )
            html_ver = html_ver.replace('<table ','<table width="100%" ')
            return html_ver

        snapshot_HTML = HTML(
            value = snapshot_HTML_val(query=widg_txtbx_querystring.value, view_snapshot_sitelimit=int(widg_dropdown_sitesnum.value)),
            layout = Layout(max_height='30%',
                           overflow='auto')
        )


        ##### Events

        def widg_but_querystring_on_click(b):
            #global snapshot_HTML
            #snapshot = newsapi.headlines_summary(view = 'snapshot', country='gb', query=widg_txtbx_querystring.value, view_snapshot_sitelimit=int(widg_dropdown_sitesnum.value)).drop(['url'], axis=1)

            snapshot_HTML.value = snapshot_HTML_val(query=widg_txtbx_querystring.value, view_snapshot_sitelimit=int(widg_dropdown_sitesnum.value))


        widg_but_querystring.on_click(widg_but_querystring_on_click)

        # Putting the widgets into a dashboard

        self.dashboard = VBox([
                Box([
                    # Title box
                    Box([newsapi_title,perd], layout = Layout(flex_flow='column',
                                                              justify_content='flex-start',
                                                              align_items='center',
                                                              border=borders
                                                              )),
                    # Options and run button box
                    Box([
                        Box([HTML("<br>")]),
                        Box([widg_txtbx_querystring], layout = Layout(align_self='flex-start', flex = '1 1 auto')),
                        Box([widg_dropdown_sitesnum], layout = Layout(align_self='flex-start', flex = '1 1 auto')),
                        Box([HTML("<br>")], layout = Layout(flex = '4 1 auto')),
                        Box([widg_but_querystring], layout = Layout(flex = '1 1 auto'))
                        ],
                        layout = Layout(flex_flow='column',
                                        justify_content='flex-start',
                                        align_items='center',
                                        border=borders
                                        )
                    )
                    ],
                    layout = Layout(flex_flow='row',
                                    justify_content='space-around',
                                    border=borders,
                                    height='40%'
                                    )
                ),
                HTML("<hr>"),
                HTML('<i><p style="color:red;">Awaiting functionality to choose sites........</p></i>'),
                HTML("<hr>"),
                snapshot_HTML,
                HTML("<hr>")
            ],layout = Layout(border='groove', height = '600px'))

    def display_dash(self):
        display(self.dashboard)

