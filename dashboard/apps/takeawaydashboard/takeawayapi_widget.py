#!/usr/bin/env python
# coding: utf-8

# In[12]:


import requests
import urllib
import json
from PIL import Image
from io import BytesIO
import pandas as pd
import smtplib, ssl
import ipywidgets as widgets
import re

###############

def coordslookup(placename):
    placename=urllib.parse.quote(placename)
    coordsurl='https://nominatim.openstreetmap.org/search?q='+placename+'&format=json&polygon=1&addressdetails=1'
    coordsresponse=requests.get(coordsurl, timeout=None)
    jdatacoords=json.loads(coordsresponse.content)
    
    if jdatacoords==[]:
        return('error',0,0)
    else:
        name=jdatacoords[0]['display_name']
        lat=jdatacoords[0]['lat']
        lon=jdatacoords[0]['lon']
    return(name, lat,lon)

###############

def restaurantlookup(lat,lon,keyword):
    restauranturl='https://maps.googleapis.com/maps/api/place/nearbysearch/json?location='+lat+','+lon+'&rankby=distance&type=restaurant&keyword='+keyword+'&opennow&key=AIzaSyBLRqyAFp7VFXY6GZk2GO-GH9yoGBx_2K0'
    restaurantresponse=requests.get(restauranturl)
    jdatarestaurant=json.loads(restaurantresponse.content)['results']
    shops=[]
    j=1
    for i in jdatarestaurant:
        if j <6:
            shops.append({'Label':j,'Name':i['name'],'Lat':i['geometry']['location']['lat'],'Lon':i['geometry']['location']['lng'],'Placeid':i['place_id']})
            j+=1
    return shops

###############

def mapmaker(lat,lon,markers):
    mapurl='https://maps.googleapis.com/maps/api/staticmap?size=600x300&maptype=roadmap'+markers+'&key=AIzaSyBLRqyAFp7VFXY6GZk2GO-GH9yoGBx_2K0'
    mapresponse=requests.get(mapurl)
    img = mapresponse.content
    return img

###############

def furtherdetails(placeid):
    resp=requests.get('https://maps.googleapis.com/maps/api/place/details/json?place_id='+placeid+'&fields=name,rating,formatted_phone_number,website&key=AIzaSyBLRqyAFp7VFXY6GZk2GO-GH9yoGBx_2K0')
    data=json.loads(resp.content)
    return(data['result'])

###############

def takeawaymodule():
    label1=widgets.Label('Enter your location:')
    text=widgets.Text()
    options=widgets.Dropdown(options=['Show me all takeaways', 'Fish & Chips', 'Pizza','Burgers','Indian','Chinese','Sushi','Chicken'])
    gobutton=widgets.Button(description='Find me a takeaway!',button_style='success')
    topbox=widgets.HBox([label1,text,options,gobutton])
    label2=widgets.Label()
    middlebox=widgets.HBox([label2])
    radios=widgets.RadioButtons()
    hidden=widgets.Layout(visibility = 'hidden')
    maptile=widgets.Image(layout = hidden)
    dbutton=widgets.Button(layout = hidden)
    d1=widgets.Label()
    d2=widgets.Label()
    d3=widgets.Label()
    d4=widgets.Label()
    detailsbox=widgets.VBox([dbutton,d1,d2,d3,d4])
    
    bottombox=widgets.HBox([maptile,widgets.VBox([radios,detailsbox])])
    groove=widgets.Layout(border='groove')
    box=widgets.VBox([topbox,middlebox,bottombox],layout=groove)
    

    def takeaway_placename_entered(btn_object):
        d1.value=''
        d2.value=''
        d3.value=''
        d4.value=''
        placename=text.value
        name=coordslookup(placename)[0]
        lat=coordslookup(placename)[1]
        lon=coordslookup(placename)[2]
        if name == 'error':
            label2.value='Sorry, the location you entered could not be found. Please try again.'
        else:
            label2.value='Your closest takeaways (currently open):'
            keyword=options.value
            if keyword=='Show me all takeaways':
                keyword=''
            top5=restaurantlookup(lat,lon,keyword)
            markers='&markers=color:red%7C'+str(lat)+','+str(lon)
            stores=[]
            for shop in top5:
                markers=markers+'&markers=color:white%7Clabel:'+str(shop['Label'])+'%7C'+str(shop['Lat'])+','+str(shop['Lon'])
                stores.append(str(shop['Label'])+': '+shop['Name'][:30])
            radios.options=stores
            img=mapmaker(lat,lon,markers)
            visible = widgets.Layout(visibility = 'visible',align_self='center')
            maptile.value=img
            maptile.layout=visible
            dbutton.description='Show details'
            dbutton.layout=visible

            def show_details(btn_object):
                label=int(radios.value[0])
                placeid=''
                for shop in top5:
                    if shop['Label']==label:
                        placeid=shop['Placeid']
                if placeid != '':
                    deets=furtherdetails(placeid)
                    if 'name' in deets:
                        d1.value=deets['name']
                    else:
                        d1.value=''
                    if 'rating' in deets:
                        d2.value='Average rating: '+str(deets['rating'])
                    else:
                        d2.value=''
                    if 'formatted_phone_number' in deets:
                        d3.value='Phone: '+str(deets['formatted_phone_number'])
                    else:
                        d3.value=''
                    if 'website' in deets:
                        d4.value='Website: '+re.findall('^(?:http)?s?:?(?:\/\/)?[^\/]+',deets['website'])[0]
                    else:
                        d4.value=''
                   
            dbutton.on_click(show_details)
            detailsbox.layout = widgets.Layout(border='solid')
            
    gobutton.on_click(takeaway_placename_entered)
    
    return(box)
################


# In[ ]:




