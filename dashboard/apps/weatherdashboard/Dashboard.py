#!/usr/bin/env python
# coding: utf-8

# In[1]:


import requests
import pandas
import json
import matplotlib.pyplot as plt
import ipywidgets as widgets
import urllib
from IPython.display import display
import csv
import datetime
from PIL import Image


###############

def coordslookup(placename):
    placename=urllib.parse.quote(placename)
    coordsurl='https://nominatim.openstreetmap.org/search?q='+placename+'&format=json&polygon=1&addressdetails=1'
    coordsresponse=requests.get(coordsurl, timeout=None)
    jdatacoords=json.loads(coordsresponse.content)
    
    if jdatacoords==[]:
        return('error',0,0)
    else:
        name=jdatacoords[0]['display_name']
        lat=jdatacoords[0]['lat']
        lon=jdatacoords[0]['lon']
    
    return(name, lat,lon)

###############

def timezonelookup(lat,lon):
    
    timezoneurl='http://api.timezonedb.com/v2.1/get-time-zone?key=NT7J58A6TIE9&format=json&by=position&lat='+lat+'&lng='+lon
    timezoneresponse=requests.get(timezoneurl)
    jdatatime=json.loads(timezoneresponse.content) 
    secsoffset=jdatatime['gmtOffset']
    hoursoffset=secsoffset/3600
    
    return(secsoffset, hoursoffset)

###############

def weatherlookup7days(lat,lon, secsoffset):

    weathercodesdict={} 
    with open('weathercodesdict.csv') as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        for row in csvReader:
            weathercodesdict.update({row[0]:row[1]})


    weatherurl='https://api-metoffice.apiconnect.ibmcloud.com/metoffice/production/v0/forecasts/point/daily?excludeParameterMetadata=false&includeLocationName=true&latitude='+lat+'&longitude='+lon
    weatherresponse=requests.get(weatherurl, 
                                 headers={'X-IBM-Client-Id':'7b48aa45-7aae-4957-9beb-9920bb420d05', 
                                          'X-IBM-Client-Secret':'I4eT6xP5xW8wI0pM3oI1eX5yC1sV8jY4dO0yU5uA5fK5bW1dF8', 
                                          'accept':'application/json'})
    jdataweather=json.loads(weatherresponse.content)
    timeseries=jdataweather['features'][0]['properties']['timeSeries']
    
    boxes=[]

    for day in range(1,len(timeseries)):
            
            dt=timeseries[day]['time']
            timestamp=datetime.datetime(int(dt[0:4]),int(dt[5:7]),int(dt[8:10]))
            daycode=timeseries[day]['daySignificantWeatherCode']
            weathericon = open('WeatherIcons/'+str(daycode)+'.png', "rb").read()
            maxdaytemp=str(round(timeseries[day]['dayMaxScreenTemperature']))
            minnighttemp=str(round(timeseries[day]['nightMinScreenTemperature']))
            probofrainday=timeseries[day]['dayProbabilityOfRain']
            probofrainnight=timeseries[day]['nightProbabilityOfRain']
            probofrain=str(round((probofrainday+probofrainnight)/2))
            box_layout = widgets.Layout(border='solid',min_width='130px')
            boxes.append(widgets.VBox([widgets.Label(value=str(timestamp.date()),align_content='center'),widgets.Image(value=weathericon, width=100, height=100),widgets.Label(value='Max: '+maxdaytemp+'°C'),widgets.Label(value='Min: '+minnighttemp+'°C'),widgets.Label(value='Chance of rain: '+probofrain+'%')],layout=box_layout))

    return(boxes)

############## 24 HOUR WEATHER FUNCTION ###############
            
def weatherlookup24h(lat, lon, secsoffset):
    
    weathercodesdict={} 
    with open('weathercodesdict.csv') as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        for row in csvReader:
            weathercodesdict.update({row[0]:row[1]})
            
    weatherurl='https://api-metoffice.apiconnect.ibmcloud.com/metoffice/production/v0/forecasts/point/hourly?excludeParameterMetadata=false&includeLocationName=true&latitude='+lat+'&longitude='+lon
    weatherresponse=requests.get(weatherurl, 
                                 headers={'X-IBM-Client-Id':'7b48aa45-7aae-4957-9beb-9920bb420d05', 
                                          'X-IBM-Client-Secret':'I4eT6xP5xW8wI0pM3oI1eX5yC1sV8jY4dO0yU5uA5fK5bW1dF8', 
                                          'accept':'application/json'})
    jdataweather=json.loads(weatherresponse.content)
    timeseries=jdataweather['features'][0]['properties']['timeSeries']

    boxes=[]

    for hour in range(1,25):
            dt=timeseries[hour]['time']
            timestamp=datetime.datetime(int(dt[0:4]),int(dt[5:7]),int(dt[8:10]),int(dt[11:13]),int(dt[14:16]))+datetime.timedelta(0,int(secsoffset))            
            weathercode=timeseries[hour]['significantWeatherCode']
            weathericon=open('WeatherIcons/'+str(weathercode)+'.png', "rb").read()
            temp=str(round(timeseries[hour]['screenTemperature']))
            feelslike=str(round(timeseries[hour]['feelsLikeTemperature']))
            probofrain=str(timeseries[hour]['probOfPrecipitation'])
            box_layout = widgets.Layout(border='solid',min_width='130px')
            boxes.append(widgets.VBox([widgets.Label(value=str(timestamp.time()),align_content='center'),widgets.Image(value=weathericon, width=100, height=100),widgets.Label(value='Temperature: '+temp+'°C'),widgets.Label(value='Feels like: '+feelslike+'°C'),widgets.Label(value='Chance of rain: '+probofrain+'%')],layout=box_layout))

    return(boxes)

###############

def main():
    toggle=widgets.ToggleButtons(options=['7 Day Forecast','24 Hour Forecast'],tooltips=['A 7 day weather forecast for the selected location', 'A 24 hour weather forecast for the selected location'])
    label1=widgets.Label('Enter location:')
    text=widgets.Text()
    gobutton=widgets.Button(description='Enter',button_style='success')
    topbox=widgets.HBox([label1, text, toggle, gobutton])
    label2=widgets.Label()
    label3=widgets.Label()
    label4=widgets.Label()
    label5=widgets.Label()
    bottombox=widgets.HBox([label2, widgets.VBox([label3, widgets.HBox([label4, label5])])])   
    box=widgets.VBox([topbox,bottombox])
    display(box)
    carousel=widgets.Box()
    display(carousel)

    def placename_entered(btn_object):
        placename=text.value
        name=coordslookup(placename)[0]
        lat=coordslookup(placename)[1]
        lon=coordslookup(placename)[2]
        if name == 'error':
            label2.value='Sorry, the location you entered could not be found. Please try again.'
            label3.value=''
            label4.value=''
            label5.value=''
            carousel.children=[widgets.Label('')]
        else:
            label2.value='Showing weather for:'
            label3.value=name
            label4.value='(Latitude: '+str(round(float(lat),2))+', Longitude: '+str(round(float(lon),2))+','
            hoursoffset=timezonelookup(lat,lon)[1]
            secsoffset=timezonelookup(lat,lon)[0]
            if hoursoffset >= 0:
                label5.value='Timezone: GMT+'+str(hoursoffset)+')'
            else:
                label5.value='Timezone: GMT'+str(hoursoffset)+')'
            if toggle.value == '7 Day Forecast':
                boxes=weatherlookup7days(lat,lon, secsoffset)
            elif toggle.value == '24 Hour Forecast':
                boxes=weatherlookup24h(lat,lon, secsoffset)
            carousel.overflow='scroll'
            carousel.children=[i for i in boxes]
            
    gobutton.on_click(placename_entered)

#######################



