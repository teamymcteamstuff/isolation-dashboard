import pymysql.cursors
import hashlib


# Putting static creds in here as they wont change
def db_connect():
    connection = pymysql.connect(host='db4free.net',
                                 port=3306,
                                 user='isodashuser',
                                 password='dairymilkdaim2',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    return connection


#Check for validity of user credentials. Returns 0 or 1: 0 = invalid credentials. 1 = valid credentials.
def login(username, password):
    connection = db_connect()
    try:
        with connection.cursor() as cursor:
            sql = """SELECT EXISTS (
                    SELECT * FROM `isodashdb`.`users` 
                    WHERE (username = %(username)s AND password_sha2 = %(password)s))"""
            sql_params = {'username': username, 'password': sha256_string(password)}
            cursor.execute(sql, sql_params)
            return list(dict.values(cursor.fetchone()))[0]
        connection.commit()
    finally:
        connection.close()


# Create a new user if the username does not already exist
# Enforce other validation in the UI:
#   username: max 20 chars
#   password: whatever we want, and if ok will be sha256'd here before db commit
def create_user(uname_in, pw_in):
    connection = db_connect()
    try:
        with connection.cursor() as cursor:
            sql = "INSERT INTO `isodashdb`.`users` (`username`, `password_sha2`) VALUES (%(uname_in)s, %(pw_in)s)"
            sql_params = {'uname_in': uname_in, 'pw_in': sha256_string(pw_in)}
            cursor.execute(sql, sql_params)
        connection.commit()
        return "success"
    # Catch if username already exists, could do this in ajax in future
    except pymysql.err.IntegrityError as e:
        return e
    finally:
        connection.close()


# Delete user if they exist
# UI to enforce user can only change details for account they are currently logged in as
def delete_user(uname_in):
    connection = db_connect()
    try:
        with connection.cursor() as cursor:
            sql = "DELETE FROM `isodashdb`.`users` WHERE username=%(uname_in)s"
            sql_params = {'uname_in': uname_in}
            cursor.execute(sql, sql_params)
        connection.commit()
    finally:
        connection.close()


# Update username if it exists
# UI to enforce user can only change details for account they are currently logged in as
def update_username(old_uname, new_uname):
    connection = db_connect()
    try:
        with connection.cursor() as cursor:
            sql = "UPDATE `isodashdb`.`users` SET username = %(new_uname)s WHERE username = %(old_uname)s;"
            sql_params = {'new_uname': new_uname, 'old_uname': old_uname}
            cursor.execute(sql, sql_params)
        connection.commit()
    finally:
        connection.close()


# UI to enforce user can only change details for account they are currently logged in as
def update_password(uname, new_pass):
    connection = db_connect()
    try:
        with connection.cursor() as cursor:
            sql = "UPDATE `isodashdb`.`users` SET password_sha2 = %(new_pass)s WHERE username = %(uname)s;"
            sql_params = {'uname': uname, 'new_pass': sha256_string(new_pass)}
            cursor.execute(sql, sql_params)
        connection.commit()
    finally:
        connection.close()


def sha256_string(pass_in):
    sha_signature = hashlib.sha256(pass_in.encode()).hexdigest()
    return sha_signature

