import db_util.sql as db

'''
    Example params:
   {'username': 'luke', 'date': '1589196489.6623218', 'name': 'potato', 'serving_qty': 1, 'serving_unit': 'Potato 
   medium', 'calories': 492.03, 'total_fat': 0.57, 'saturated_fat': 0.15, 'salt': 0.1, 'cholesterol': 0, 
   'total_carbohydrate': 111.75, 'fibre': 15, 'sugar': 6.25, 'protein': 13, 'potassium': 2700} 
'''
# Keeping a record of all items eaten and their nutritional values, totting up the totals can be done by dashboard code
# Want to do it this way rather than keeping nutritional totals so user can see what specific foods they ate too
# ** is cos im passing a dictionary as a parameter
def add_nutrition(params):
    connection = db.db_connect()
    try:
        with connection.cursor() as cursor:
            sql = """
                    INSERT INTO `isodashdb`.`nutrition`(`username`, `date`, `food_name`, `serving_qty`, `serving_unit`, 
                    `calories`, `total_fat_g`, `saturated_fat_g`, `salt_g`, `cholesterol_mg`, `total_carbs_g`, 
                    `fibre_g`, `sugar_g`, `protein_g`, `potassium_mg`) VALUES (%(username)s, %(date)s, %(food_name)s, 
                    %(serving_qty)s, %(serving_unit)s, %(calories)s, %(total_fat)s, %(saturated_fat)s, %(salt)s, 
                    %(cholesterol)s, %(total_carbs)s, %(fibre)s, %(sugar)s, %(protein)s, %(potassium)s)
            """
            sql_params = {'username': params['username'], 'date': params['date'], 'food_name': params['name'],
                          'serving_qty': params['serving_qty'], 'serving_unit': params['serving_unit'],
                          'calories': params['calories'], 'total_fat': params['total_fat'],
                          'saturated_fat': params['saturated_fat'], 'salt': params['salt'],
                          'cholesterol': params['cholesterol'], 'total_carbs': params['total_carbohydrate'],
                          'fibre': params['fibre'], 'sugar': params['sugar'], 'protein': params['protein'],
                          'potassium': params['potassium']}
            cursor.execute(sql, sql_params)
        connection.commit()
        return "success"
    finally:
        connection.close()


#Date must be in the format "11th May 2020", will be called by dashboard code
def load_day(username, date):
    connection = db.db_connect()
    try:
        with connection.cursor() as cursor:
            sql = "SELECT * FROM `isodashdb`.`nutrition` WHERE username=%(username)s AND " \
                  "                                             FROM_UNIXTIME(date,'%%D %%M %%Y')=%(date)s"
            sql_params = {'username': username, 'date': date}
            cursor.execute(sql, sql_params)
        connection.commit()
        results = list(cursor.fetchall())
        return results
    finally:
        connection.close()
    pass

