import time
import datetime
import db_util.sql as db
import pymysql


# Add 'plain' value associated with a game to the gamedeals database, labelled with the user
# plain, added, username
def add_game(plain, username):
    connection = db.db_connect()
    try:
        with connection.cursor() as cursor:
            sql = """INSERT INTO `isodashdb`.`game_wishlist`(`plain`, `username`)
                    VALUES (%(plain)s,%(username)s)"""
            sql_params = {'plain': plain, 'username': username}
            cursor.execute(sql, sql_params)
        connection.commit()
        return "success"
    # Catch if entry already exists, could do this in ajax in future
    except pymysql.err.IntegrityError as e:
        return e
    finally:
        connection.close()


def remove_game(plain, username):
    connection = db.db_connect()
    try:
        with connection.cursor() as cursor:
            sql = "DELETE FROM `isodashdb`.`game_wishlist` WHERE username=%(username)s AND plain=%(plain)s"
            sql_params = {'username': username, 'plain': plain}
            cursor.execute(sql, sql_params)
        connection.commit()
    finally:
        connection.close()


def get_user_wishlist(username):
    connection = db.db_connect()
    try:
        with connection.cursor() as cursor:
            sql = "SELECT * FROM `isodashdb`.`game_wishlist` WHERE username=%(username)s"
            sql_params = {'username': username}
            cursor.execute(sql, sql_params)
        connection.commit()
        results = list(cursor.fetchall())
        return results
    finally:
        connection.close()
    pass

