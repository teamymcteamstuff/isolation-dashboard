# Isolation Dashboard Readme

This contains a summary of the structure of the repo including conventions.

---

## Repo Structure

1. api_modules contains all the modules to query different apis
2. dashboard contains the modules to build to jupyter dashboard including functionality written elsewhere in this repo

## Api Modules

**Each api modules folder should have the following:**

Include some kind of text file (e.g. .txt, .md etc) with the following details:

1. Website taken from 
2. Summary of what different data is there
3. Endpoints used
4. An example of how you expect your module to be used in the dashboard. This might be a diagram of how would visualise this functionality (e.g. use paint to make a mockup).

## Naming Structure - PEP8

Table of naming styles located at [link](https://realpython.com/python-pep8/#naming-styles)

1. **(ESSENTIAL)** Modules (aka python files):
	- lowercase with underscores
	- e.g. my_module.py
2. **(ESSENTIAL)** Packages (aka folders):
	- lowercase with no word separation
	- e.g. mypackage
3. *(DESIRABLE)* Variables & Functions
	- lowercase with underscores
	- e.g. my_function, my_variable
	