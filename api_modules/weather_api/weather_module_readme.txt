### ~~ Weather API ReadMe ~~ ###


Data is taken from Met Office weather API:
Hourly data: https://metoffice.apiconnect.ibmcloud.com/metoffice/production/node/175
- Gives weather every hour at the specified location (used for 24 forecast).

Daily data: https://metoffice.apiconnect.ibmcloud.com/metoffice/production/node/174
- Gives summarised daily weather for yesterday, today plus the next 6 days at the specified location (used for 7 day forecast).

Supplemented w/ data from:
OpenStreetMap nominatim API: https://nominatim.openstreetmap.org/search?q=placename&format=json&polygon=1&addressdetails=1
-Returns coordinates of a given input location in text. Will accept any granularity of input from country level, down to county/city/town/street/address/postcode. (Used to supply coords of interest to met office weather API queries).

Timezone DB API:http://api.timezonedb.com/v2.1/get-time-zone?key=NT7J58A6TIE9&format=json&by=position&lat=0.000&lng=12.345
-Looks up the timezone of the input coordinates and returns the difference (in seconds) from Zulu time (used to convert the output times from met office 24 forecast API into local time at location of interest).


Summary of use of this module in isolation dashboard:
-User enters location of interest
-Chooses between 24h or 7 day forecast
-Presses enter
-Nominatim API looks up the coords of that location
-Timezonedb API looks up timezone of those coords
-Location resolved by Nominatim is displayed to the screen to ensure it's correct (eg Birmingham, UK not Birmingham Alabama)
-Chosen forecast is displayed by the day/by the hour
-Weather icons or gifs are shown corresponding to the output weather code from met office API 
-Timestamps are converted to local time based on output of Timezone db api request
