#!/usr/bin/env python
# coding: utf-8

# In[3]:


import requests #used for http get requests to the APIs
import json #used to parse json blobs
import csv #used to read csv file containing weather codes
import re #used for regex
import urllib #used for url encoding
import datetime #used for parsing timestamps / changing timezone

############## COORDS LOOKUP FUNCTION ##############

def coordslookup(placename):
    
    #url encode the input
    placename=urllib.parse.quote(placename)

    #create osm nominatim api url using the inputted name
    coordsurl='https://nominatim.openstreetmap.org/search?q='+placename+'&format=json&polygon=1&addressdetails=1'

    #make http get request to nominatim and save the response
    coordsresponse=requests.get(coordsurl, timeout=None)

    #parse the json response into a python dict
    jdatacoords=json.loads(coordsresponse.content)

    #use jpath expressions to extract the place name / lat / lon returned into their own variables
    name=jdatacoords[0]['display_name']
    lat=jdatacoords[0]['lat']
    lon=jdatacoords[0]['lon']
    return(name, lat,lon)


############## TIMEZONE LOOKUP FUNCTION ##############

def timezonelookup(lat,lon):
    
    #make a request to timezone API to find timezone of these coords
    #key used here can be used up to once per second
    timezoneurl='http://api.timezonedb.com/v2.1/get-time-zone?key=NT7J58A6TIE9&format=json&by=position&lat='+lat+'&lng='+lon
    timezoneresponse=requests.get(timezoneurl)
    #parse the json response into a python dict
    jdatatime=json.loads(timezoneresponse.content) 
    #find offset from Zulu time
    secsoffset=jdatatime['gmtOffset']
    hoursoffset=secsoffset/3600
    
    return(secsoffset, hoursoffset)

############## 7 DAY WEATHER FUNCTION ###############

def weatherlookup7days(lat,lon):
    
    #creating a blank dictionary to feed weather codes into
    weathercodesdict={} 

    #opening csv with table of weather codes > desciptions, reading in each line
    #then using these to create a python dict
    #data from https://metoffice.apiconnect.ibmcloud.com/metoffice/production/node/264

    with open('weathercodesdict.csv') as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        for row in csvReader:
            weathercodesdict.update({row[0]:row[1]})

    #create metoffice api url using the returend lat/lon
    weatherurl='https://api-metoffice.apiconnect.ibmcloud.com/metoffice/production/v0/forecasts/point/daily?excludeParameterMetadata=false&includeLocationName=true&latitude='+lat+'&longitude='+lon

    #make http get request to metoffice site and save the response
    #NB the values in the headers section correspond to the API key I generated for this site
    #up to 360 calls per day can be made using these creds
    weatherresponse=requests.get(weatherurl, 
                                 headers={'X-IBM-Client-Id':'7b48aa45-7aae-4957-9beb-9920bb420d05', 
                                          'X-IBM-Client-Secret':'I4eT6xP5xW8wI0pM3oI1eX5yC1sV8jY4dO0yU5uA5fK5bW1dF8', 
                                          'accept':'application/json'})

    #parse the json response into a python dict
    jdataweather=json.loads(weatherresponse.content)

    #extract the 7 day data
    timeseries=jdataweather['features'][0]['properties']['timeSeries']

    print('\nHere is your 7 day forecast: \n')

    #data returned contains weather for yesterday, today and the next 6 days
    #seems pointless plotting yesterday's weather 
    #so loop through each date, starting at today, and extract useful values
    for day in range(1,len(timeseries)):
            
            dt=timeseries[day]['time']
            timestamp=datetime.datetime(int(dt[0:4]),int(dt[5:7]),int(dt[8:10]))

            #found details on each parameter at
            #https://www.metoffice.gov.uk/binaries/content/assets/metofficegovuk/pdf/data/global-spot-data-daily.pdf
            daycode=timeseries[day]['daySignificantWeatherCode']
            daysummary=re.search('[^(]+',weathercodesdict[str(daycode)]).group()
            nightcode=timeseries[day]['nightSignificantWeatherCode']
            nightsummary=re.search('[^(]+',weathercodesdict[str(nightcode)]).group()
            maxdaytemp=timeseries[day]['dayMaxScreenTemperature']
            minnighttemp=timeseries[day]['nightMinScreenTemperature']
            probofrainday=timeseries[day]['dayProbabilityOfRain']
            probofrainnight=timeseries[day]['nightProbabilityOfRain']

            print('\n --- ',timestamp.date(),' --- ')


            #refer back to dictionary of weather codes created earlier
            #look up the code for each day and return the summary it corresponds to
            print('Day summary: ',daysummary)
            print('Night summary: ',nightsummary)

            #print temp rounded to nearest integer - it's given to 4dp by default which feels excessive
            print('Temperature: \n Max:',int(round(maxdaytemp)),'°C \n Min:',int(round(minnighttemp)),'°C')
            print('Chance of rain: \n Day:',probofrainday, '% \n Night:',probofrainnight,'%')


############## 24 HOUR WEATHER FUNCTION ###############
            
def weatherlookup24h(lat, lon):
    
    #creating a blank dictionary to feed weather codes into
    weathercodesdict={} 

    #opening csv with table of weather codes > desciptions, reading in each line
    #then using these to create a python dict
    #data from https://metoffice.apiconnect.ibmcloud.com/metoffice/production/node/264

    with open('weathercodesdict.csv') as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        for row in csvReader:
            weathercodesdict.update({row[0]:row[1]})
    
    #create metoffice api url using the returend lat/lon
    weatherurl='https://api-metoffice.apiconnect.ibmcloud.com/metoffice/production/v0/forecasts/point/hourly?excludeParameterMetadata=false&includeLocationName=true&latitude='+lat+'&longitude='+lon
    
    #make http get request to metoffice site and save the response
    #NB the values in the headers section correspond to the API key I generated for this site
    #up to 360 calls per day can be made using these creds
    weatherresponse=requests.get(weatherurl, 
                                 headers={'X-IBM-Client-Id':'7b48aa45-7aae-4957-9beb-9920bb420d05', 
                                          'X-IBM-Client-Secret':'I4eT6xP5xW8wI0pM3oI1eX5yC1sV8jY4dO0yU5uA5fK5bW1dF8', 
                                          'accept':'application/json'})
    #parse the json response into a python dict
    jdataweather=json.loads(weatherresponse.content)
        
    #Extract the 24 hour data
    timeseries=jdataweather['features'][0]['properties']['timeSeries']
    
    print('\nHere is your 24 hour forecast: \n')

    #data returned contains weather for the previous hour, current hour and hourly data for the next several days
    #loop through each hour for the next 24, starting at the current hour, and extract useful values
    for hour in range(1,25):
            #datetime comes out in format eg 2020-04-12T00:00Z 
            #use datetime module to create a 'datetime' object
            #use this with datetime.timedelta to change timezone using value created earlier
            dt=timeseries[hour]['time']
            timestamp=datetime.datetime(int(dt[0:4]),int(dt[5:7]),int(dt[8:10]),int(dt[11:13]),int(dt[14:16]))+datetime.timedelta(0,int(secsoffset))            

            #found details on each parameter at
            #https://www.metoffice.gov.uk/binaries/content/assets/metofficegovuk/pdf/data/global-spot-data-daily.pdf
            weathercode=timeseries[hour]['significantWeatherCode']
            summary=re.search('[^(]+',weathercodesdict[str(weathercode)]).group()
            
            temp=timeseries[hour]['screenTemperature']
            feelslike=timeseries[hour]['feelsLikeTemperature']
            probofrain=timeseries[hour]['probOfPrecipitation']

            print('\n --- ',timestamp.time(),' --- ')

            print('Summary: ',summary)
            #print temp rounded to nearest integer - it's given to 4dp by default which feels excessive
            print('Temperature: ',int(round(temp)),'°C \nFeels like:',int(round(feelslike)),'°C')
            print('Chance of rain: ',probofrain, '%')
    
    
    
    
############### MAIN ###############

#take user input of place name      
placename=input('Enter the location you would like to see weather for:  ')

#use coordslookup function to find coords for inputted name
name=coordslookup(placename)[0]
lat=coordslookup(placename)[1]
lon=coordslookup(placename)[2]

#print place name and coords to screen
print('\nViewing weather for: \n',name,'\nLat: ',lat,'\nLon: ',lon)

hoursoffset=timezonelookup(lat,lon)[1]
secsoffset=timezonelookup(lat,lon)[0]
    
if hoursoffset >= 0:
    print('Timezone: GMT+',hoursoffset)
else:
    print('Timezone: GMT',hoursoffset)

choice=input('\nWould you like to view weather for 24 hours (type \'1\') or 7 days (type \'2\')? ')

if int(choice) == 1:
    weatherlookup24h(lat,lon)
    
if int(choice) == 2:
    weatherlookup7days(lat,lon)

