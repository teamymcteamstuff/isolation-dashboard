#!/usr/bin/env python
# coding: utf-8

# # Newsapi.org libraries
# 
# ### Try these example functions:
#     headlines_summary(country='gb', query='corona',view = 'snapshot')
#     
#     headlines_summary(country='gb', query='corona',view = 'raw')
#     
#     headlines_summary(country='gb', query='corona',view = 'meta')
#     
#     headlines_summary(country='gb', query='corona',view = 'titles')
#     
#     headlines_summary(country='gb', query='corona',view = 'sites')
# 
# ### Functions available:
#     headlines_summary various summaries of news articles from newsapi.org
#         it takes the following arguments:
#             category
#             country
#             query
#             pageSize - number of results to bring back from newsapi.com
#             view - this should be 'snapshot', 'raw', 'meta', 'titles', 'sites'
#             view_snapshot_sitelimit - limits the number of sites for snapshot view

# In[1]:


# importing libs

from IPython.display import clear_output

import requests
import pandas as pd
import datetime
import re
get_ipython().system('pip install tld')
from tld import get_fld

clear_output()


# In[2]:


# Errors
errors = {'category not valid':'Error: category not valid',
          'country not valid':'Error: country not valid',
          'page_size not valid':'Error: page_size not valid',
          'view not valid':'Error: view not valid',
          'view_snapshot_sitelimit not valid':'Error: view_snapshot_sitelimit not valid'
         }


# In[3]:


# Headlines summary function

# Possible settings for 'view' argument:
# view = 'raw', gives you the raw data
# view = 'meta', gives you just the metadata
# view = 'sites', will give you all the sites returned from the headlines
# view = 'titles', will give you all the headline titles as a summary
# view = 'snapshot', will give you a snapshot of X (default=5) headlines, one from each of the top appearing news sites (bbc, dailymail,) or chosen news sites
def headlines_summary(category='', country='', query='', page_size=100, view='', view_snapshot_sitelimit=5, debug=False):
    url = ('http://newsapi.org/v2/top-headlines?apiKey=68da2775de6544769182d483a0d2c86b')
    
    def fld(url):
        try:
            return get_fld(url)
        except:
            return url
    
    # Category search section
    if category != '':
        valid_categories =['business',
                           'entertainment',
                           'general',
                           'health',
                           'science',
                           'sports',
                           'technology']
        if category not in valid_categories:
            return print(errors['category not valid'])
        else:
            url = url + '&category=' + category
            
    # Country search section
    if country != '':
        # UK is 'gb'
        valid_countries =['ae', 'ar', 'at', 'au', 'be', 'bg', 'br', 'ca', 'ch', 'cn', 'co', 'cu', 'cz', 'de', 'eg', 'fr', 'gb', 'gr', 'hk', 'hu', 'id', 'ie', 'il', 'in', 'it', 'jp', 'kr', 'lt', 'lv', 'ma', 'mx', 'my', 'ng', 'nl', 'no', 'nz', 'ph', 'pl', 'pt', 'ro', 'rs', 'ru', 'sa', 'se', 'sg', 'si', 'sk', 'th', 'tr', 'tw', 'ua', 'us', 've', 'za']
        if country not in valid_countries:
            return print(errors['country not valid'])
        else:
            url = url + '&country=' + country
    
    # Query search section
    if query != '':
        url = url + '&q=' + query
    
    # Pagesize section
    if page_size != 20:
        if page_size not in range(101):
            return print(errors['page_size not valid'])
        else:
            url = url + '&pageSize=' + str(page_size)
    
    if debug == True:
        print(url)
    
    # Get the response
    response = requests.get(url)
    
    results = response.json()
    
    # Check view
    if view == '':
        return print(errors['view not valid'])
    
    elif view == 'raw':
        return results
    
    elif view == 'meta':
        summary = {}

        summary['totalResults'] = results['totalResults']
        summary['retrievedResults'] = len(results['articles'])

        return summary
    
    elif view == 'sites':
        summary = []
        sites = []
        for article in results['articles']:            
            sites.append(fld(article['url']))
            
        sites = pd.Series(sites).value_counts()
        return sites
    
    elif view == 'titles':
        titles = []
        
        for article in results['articles']:
            titles.append(article['source']['name']
                                           + ' : '
                                           + re.sub(" - [^-]+$","",article['title'])
                                          )
        
        return titles
    
    elif view == 'snapshot':
        
        if type(view_snapshot_sitelimit) != int or view_snapshot_sitelimit < 1:
            return print(errors['view_snapshot_sitelimit not valid'])
        
        snapshot = []
        
        articles = pd.DataFrame(results['articles'])
        
        articles['url_domains'] = articles['url'].apply(lambda x: fld(x))
        
        sites_summary = headlines_summary(country=country, query=query, view = 'sites')

        if view_snapshot_sitelimit > len(sites_summary):
            view_snapshot_sitelimit = len(sites_summary)
        
        if len(sites_summary.index) > view_snapshot_sitelimit-1:
            sites_num = view_snapshot_sitelimit
        else:
            sites_num = len(sites_summary) + 1

        for site in sites_summary.index[0:sites_num].to_list():
           
            first_article = articles[(articles.url_domains == site)].iloc[0]
            snapshot.append({'url_domain':first_article.url_domains,
                             'title':first_article.title,
                             'url':first_article.url,
                             'publishedAt':datetime.datetime.strptime(first_article.publishedAt, '%Y-%m-%dT%H:%M:%SZ').strftime('%Y-%m-%d@%H:%M')
                            })
        
        return pd.DataFrame(snapshot)
    
    else:
        return print(errors['view not valid'])

