import requests


# Triggered by user-entered search, returns a list of suggested items
def get_foods(input):
    headers = {
        'x-app-id': '1045881a',
        'x-app-key': '682699d9904c553bf60aa3030751b15f'
    }
    input = input.replace(" ", "+")
    response = requests.get(
        "https://trackapi.nutritionix.com/v2/search/instant?branded=true&common=true&branded_region=2&query=" \
        + input + "&self=false", headers=headers)
    response_json = response.json()
    return response_json


# Triggered when user double clicks an item from the 'common' part of the response from get_foods. 'Serving' used to
# serving_unit and serving_qty populate the serving field
def get_common_nutrition(input):
    headers = {
        'accept': 'application/json',
        'x-app-id': '1045881a',
        'x-app-key': '682699d9904c553bf60aa3030751b15f',
        'x-remote-user-id': '0',
        'Content-Type': 'application/json'
    }
    data = '{"query": ' + "\"{0}\"".format(
        input) + ', "timezone": "Europe/London", "line_delimited": "false", "use_raw_foods":"false", "use_branded_foods":"true"}'
    response = requests.post('https://trackapi.nutritionix.com/v2/natural/nutrients', headers=headers, data=data)
    response_json = response.json()
    output_dict = create_output_dict(response_json)
    return output_dict


# Triggered if user selects item in 'branded' part of response from get_foods, itemid will be the nix_item_id value returned
# serving_unit and serving_qty populate the serving field
def get_branded_nutrition(itemid):
    headers = {
        'x-app-id': '1045881a',
        'x-app-key': '682699d9904c553bf60aa3030751b15f'
    }
    response = requests.get(
        "https://trackapi.nutritionix.com/v2/search/item?nix_item_id={0}".format(itemid), headers=headers)
    response_json = response.json()
    output_dict = create_output_dict(response_json)
    return output_dict


def create_output_dict(response_json):
    print(response_json["foods"][0]["food_name"])
    output_dict = {}
    try:
        output_dict = {
            "name": response_json["foods"][0]["food_name"],
            #"tag": response_json["foods"][0]["tags"]["item"],
            "serving": response_json["foods"][0]["serving_qty"],
            "serving_unit": response_json["foods"][0]["serving_unit"],
            "calories": response_json["foods"][0]["nf_calories"],
            "total_fat": response_json["foods"][0]["nf_total_fat"],
            "saturated_fat": response_json["foods"][0]["nf_saturated_fat"],
            "salt": (response_json["foods"][0]["nf_sodium"] * 2.5) / 1000,  # convert sodium (mg) to salt (g)
            "cholesterol": response_json["foods"][0]["nf_cholesterol"],
            "total_carbohydrate": response_json["foods"][0]["nf_total_carbohydrate"],
            "fibre": response_json["foods"][0]["nf_dietary_fiber"],
            "sugar": response_json["foods"][0]["nf_sugars"],
            "protein": response_json["foods"][0]["nf_protein"],
            "potassium": response_json["foods"][0]["nf_potassium"],
            "image_url": response_json["foods"][0]["photo"]["highres"]
        }
    #Some items do not have some fields populated
    except TypeError:
        return("Error occurred creating output dictionary")

    return output_dict


