1. Website taken from: nutritionix.com
2. Data returned for each food item:
	- Name
	- Serving size
	- Serving unit (e.g. grams)
	- Calories
	- Fat
	- Saturated fat
	- Salt
	- Cholesterol
	- Carbohydrates
	- Fibre
	- Sugar
	- Protein
	- Potassium
	- URL to image
3. Endpoints used: 
	- trackapi.nutritionix.com/v2/natural/nutrients
	- trackapi.nutritionix.com/v2/search/instant
	- trackapi.nutritionix.com/v2/search/item
4. /search/instant provides user with possible options for their input, which once chosen will be provided to a query to /natural/nutrients to get a full pull of their nutritional data. /search/item returns nutritional information for branded goods, whilst /nautral/nutrients returns them for generic items.
