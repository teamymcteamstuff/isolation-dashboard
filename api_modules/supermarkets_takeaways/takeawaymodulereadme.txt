### ~~ Takeaway API ReadMe ~~ ###


Data is taken from Google maps API:
Places API: https://developers.google.com/places/web-service/intro 
- Allows you to query for places of interest near a particular location and specify parameters such as keywords and opening hours (e.g. find supermarkets near b29 6eq that are open now, or find vegetarian restaurants in cheltenham)

Maps static API: https://developers.google.com/maps/documentation/maps-static/intro 
- Given a central coordinates and coords of places of interest it will return a map centred on the central coordinate with markers showing all the places of interest. 

Supplemented w/ data from:
OpenStreetMap nominatim API: https://nominatim.openstreetmap.org/search?q=placename&format=json&polygon=1&addressdetails=1
-Returns coordinates of a given input location in text. Will accept any granularity of input from country level, down to county/city/town/street/address/postcode. (Used to supply coords of interest to Google maps API queries).

Summary of use of this module in isolation dashboard:
-User enters location of interest
-Chooses between takeaway type
-Presses go
-Nominatim API looks up the coords of that location
-Google maps API looks up 5 closest matching takeaways to that location that are currently open
-Location resolved by Nominatim plus the 5 closest takeaways are displayed on a map tile on the screen
-A drop down ox will allow the user to select one of the 5 takeaways
-More details will be displayed, such as the user rating and phone number/website link (from Google API)