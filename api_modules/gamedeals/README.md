1. Website taken from: isthereanydeal.com
2. Data returned for each game: title, old price, new price, shop name, url to page
3. Endpoints used: 
	- api.isthereanydeal.com/v01/deals/list
	- api.isthereanydeal.com/v01/search/search
4. /deals/list endpoint returns daily deals as highlighted by isthereanydeal service. /search/ endpoint returns prices of games matching input search string.
