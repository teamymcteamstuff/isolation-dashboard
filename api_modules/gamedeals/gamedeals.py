import requests
import json


# Return list of daily game deals as highlghted by isthereanydeal service
def get_daily_deals():
    endpoint = "https://api.isthereanydeal.com/v01/deals/list/?key=716483de0944a10d687c8afe8ee5a09ef23c4b3e&country" \
               "=GB&sort=price%3Aasc"
    return api_query(endpoint)


# Return data and parse out fields from a user-seeded query for game deals against a particular title
def get_deals_by_name(name):
    endpoint = "https://api.isthereanydeal.com/v01/search/search/?key=716483de0944a10d687c8afe8ee5a09ef23c4b3e&q={0}\&country=GB".format(
        name)
    return api_query(endpoint)


# Only used by above two functions as fields returned by other API endpoints differ
# 'plain' field returned can be used as the param for subsequent queries to get_historic_low_price and get_game_reviews
def api_query(endpoint):
    response = requests.get(endpoint)
    response_dict = response.json()
    output_json = []
    for i in response_dict["data"]["list"]:
        output = {'plain': i["plain"], 'title': i["title"], 'price_old': i["price_old"],
                  'price_new': i["price_new"], 'shop': i["shop"]["name"], 'urls': i["urls"]["buy"]}
        output_json.append(json.dumps(output))
    return output_json


# Can be seeded with 'plain' value which is the unique name for each game assigned by isthereanydeal
# Function will be used to enrich information displayed in daily deals and wishlists to show historic low price
def get_historic_low_price(plain):
    endpoint = "https://api.isthereanydeal.com/v01/game/lowest/?key=716483de0944a10d687c8afe8ee5a09ef23c4b3e&plains={0}" \
        .format(plain)
    response = requests.get(endpoint)
    response_dict = response.json()
    output_json = []
    for key, value in response_dict.items():
        try:
            output = {'shop_name': value[plain]['shop']['name'], 'price': value[plain]['price'], 'added': value[plain]['added']}
            output_json.append(output)
        except KeyError:
            pass
    return output_json


# Can be seeded with 'plain' value which is the unique name for each game assigned by isthereanydeal
# Function will be used to enrich information displayed in daily deals and wishlists to show Steam reviews
def get_game_reviews(plain):
    endpoint = "https://api.isthereanydeal.com/v01/game/info/?key=716483de0944a10d687c8afe8ee5a09ef23c4b3e&plains={0}" \
        .format(plain)
    response = requests.get(endpoint)
    response_dict = response.json()
    output_json = []
    for key, value in response_dict.items():
        try:
            output = {'title': value[plain]['title'],
                      'percent_reviews_positive': value[plain]['reviews']['steam']['perc_positive'],
                      'total_reviews': value[plain]['reviews']['steam']['total'], 'image': value[plain]['image']}
            output_json.append(output)
        except KeyError:
            pass
    return output_json

